<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Controller\Adminhtml\Employee;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @var \Summa\Test\Model\EmployeeFactory
     */
    private \Summa\Test\Model\EmployeeFactory $_employeeFactory;

    /**
     * @var \Summa\Test\Api\CompanyRepositoryInterface
     */
    private \Summa\Test\Api\CompanyRepositoryInterface $_companyRepositoryInterface;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Summa\Test\Model\EmployeeFactory $employeeFactory,
        \Summa\Test\Api\CompanyRepositoryInterface $_companyRepositoryInterface
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_employeeFactory = $employeeFactory;
        $this->_companyRepositoryInterface = $_companyRepositoryInterface;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('employee_id');
            $type = $this->getRequest()->getParam('type');
            $companyId = $this->getRequest()->getParam('company_id');

            $model = $this->_employeeFactory->create($type)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Employee no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $company = $this->_companyRepositoryInterface->get($companyId);
                $company->addEmployee($model->getId());

                $this->messageManager->addSuccessMessage(__('You saved the Employee.'));
                $this->dataPersistor->clear('summa_test_employee');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['employee_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Employee.'));
            }

            $this->dataPersistor->set('summa_test_employee', $data);
            return $resultRedirect->setPath('*/*/edit', ['employee_id' => $this->getRequest()->getParam('employee_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

