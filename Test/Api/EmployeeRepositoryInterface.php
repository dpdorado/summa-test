<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface EmployeeRepositoryInterface
{

    /**
     * Save Employee
     * @param \Summa\Test\Api\Data\EmployeeInterface $employee
     * @return \Summa\Test\Api\Data\EmployeeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Summa\Test\Api\Data\EmployeeInterface $employee
    );

    /**
     * Retrieve Employee
     * @param string $employeeId
     * @return \Summa\Test\Api\Data\EmployeeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($employeeId);

    /**
     * Retrieve Employee matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Summa\Test\Api\Data\EmployeeSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Employee
     * @param \Summa\Test\Api\Data\EmployeeInterface $employee
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Summa\Test\Api\Data\EmployeeInterface $employee
    );

    /**
     * Delete Employee by ID
     * @param string $employeeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($employeeId);
}

