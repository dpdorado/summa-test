<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Api\Data;

interface CompanyInterface
{

    const EMPLOYEES = 'employees';
    const NAME = 'name';
    const COMPANY_ID = 'company_id';
    const AVERAGE_AGE = 'average_age';

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId();

    /**
     * Set company_id
     * @param string $companyId
     * @return $this
     */
    public function setCompanyId($companyId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get employees
     * @return string|null
     */
    public function getEmployees();

    /**
     * Set employees
     * @param string $employees
     * @return $this
     */
    public function setEmployees($employees);

    /**
     * Get average_age
     * @return string|null
     */
    public function getAverageAge();

    /**
     * Set average_age
     * @param string $averageAge
     * @return $this
     */
    public function setAverageAge($averageAge);

    /**
     * Add employee in company
     *
     * @param string $employeeId
     * @return bool
     */
    public function addEmployee($employeeId);

    /**
     * Add employee list
     *
     * @return \Summa\Test\Model\ResourceModel\Employee\Collection
     */
    public function getEmployeeList();

    /**
     * get employee in company by employee id
     *
     * @param $employeeId
     * @return \Summa\Test\Api\Data\EmployeeInterface|null
     */
    public function searchemployee($employeeId);

    /**
     * Get average age of employees
     *
     * @param $employeeList
     * @return float
     */
    public function getAverageAgeOfEmployees($employeeList = null);
}

