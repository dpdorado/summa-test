<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Api\Data;

interface CompanySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Company list.
     * @return \Summa\Test\Api\Data\CompanyInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Summa\Test\Api\Data\CompanyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

