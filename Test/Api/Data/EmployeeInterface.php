<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Api\Data;

interface EmployeeInterface
{

    const ABILITY = 'ability';
    const LASTNAME = 'lastname';
    const AGE = 'age';
    const EMPLOYEE_ID = 'employee_id';
    const FIRSTNAME = 'firstname';
    const COMPANY_ID = 'company_id';
    const TYPE = 'type';

    /**
     * Get employee_id
     * @return string|null
     */
    public function getEmployeeId();

    /**
     * Set employee_id
     * @param string $employeeId
     * @return $this
     */
    public function setEmployeeId($employeeId);

    /**
     * Get firstname
     * @return string|null
     */
    public function getFirstname();

    /**
     * Set firstname
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname);

    /**
     * Get lastname
     * @return string|null
     */
    public function getLastname();

    /**
     * Set lastname
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname);

    /**
     * Get age
     * @return string|null
     */
    public function getAge();

    /**
     * Set age
     * @param string $age
     * @return $this
     */
    public function setAge($age);

    /**
     * Get ability
     * @return string|null
     */
    public function getAbility();

    /**
     * Set ability
     * @param string $ability
     * @return $this
     */
    public function setAbility($ability);

    /**
     * Get type
     * @return string|null
     */
    public function getType();

    /**
     * Set age
     * @param string $type
     * @return $this
     */
    public function setType($type);

    /**
     * Get company_id
     * @return string|null
     */
    public function getCompanyId();

    /**
     * Set company_id
     * @param string $companyId
     * @return $this
     */
    public function setCompanyId($companyId);
}

