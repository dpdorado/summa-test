<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Api\Data;

interface EmployeeSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Employee list.
     * @return \Summa\Test\Api\Data\EmployeeInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Summa\Test\Api\Data\EmployeeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

