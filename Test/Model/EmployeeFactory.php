<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model;

class EmployeeFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private \Magento\Framework\ObjectManagerInterface $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create config model
     *
     * @param array $sourceData
     * @return Employee
     */
    public function create($sourceData = null, $type = '')
    {
        $class = '';
        switch ($type) {
            case 'programador':
                $class = Programmer::class;
                break;
            case 'diseñador':
                $class = Designer::class;
                break;
            default:
                $class = Employee::class;
        }

        return $this->getObjectModel($class, $sourceData);
    }

    /**
     * @param $class
     * @param $sourceData
     * @return mixed|Employee
     */
    private function getObjectModel($class, $sourceData)
    {
        $object = null;
        if ($sourceData) {
            $object = $this->_objectManager->create($class, ['sourceData' => $sourceData]);
        } else {
            $object = $this->_objectManager->create($class);
        }
        return $object;
    }
}
