<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model;

use Magento\Framework\Model\AbstractModel;
use Summa\Test\Api\Data\EmployeeInterface;

class Employee extends AbstractModel implements EmployeeInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Summa\Test\Model\ResourceModel\Employee::class);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeId()
    {
        return $this->getData(self::EMPLOYEE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeId($employeeId)
    {
        return $this->setData(self::EMPLOYEE_ID, $employeeId);
    }

    /**
     * @inheritDoc
     */
    public function getFirstname()
    {
        return $this->getData(self::FIRSTNAME);
    }

    /**
     * @inheritDoc
     */
    public function setFirstname($firstname)
    {
        return $this->setData(self::FIRSTNAME, $firstname);
    }

    /**
     * @inheritDoc
     */
    public function getLastname()
    {
        return $this->getData(self::LASTNAME);
    }

    /**
     * @inheritDoc
     */
    public function setLastname($lastname)
    {
        return $this->setData(self::LASTNAME, $lastname);
    }

    /**
     * @inheritDoc
     */
    public function getAge()
    {
        return $this->getData(self::AGE);
    }

    /**
     * @inheritDoc
     */
    public function setAge($age)
    {
        return $this->setData(self::AGE, $age);
    }

    /**
     * @inheritDoc
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @inheritDoc
     */
    public function getAbility()
    {
        return $this->getData(self::ABILITY);
    }

    /**
     * @inheritDoc
     */
    public function setAbility($ability)
    {
        return $this->setData(self::ABILITY, $ability);
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId()
    {
        return $this->getData(self::COMPANY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCompanyId($companyId)
    {
        return $this->setData(self::COMPANY_ID, $companyId);
    }
}

