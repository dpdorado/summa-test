<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model;

use Magento\Framework\Model\AbstractModel;
use Summa\Test\Api\Data\CompanyInterface;

class Company extends AbstractModel implements CompanyInterface
{

    /**
     * @var \Summa\Test\Model\ResourceModel\Employee\CollectionFactory
     */
    private ResourceModel\Employee\CollectionFactory $_employeeCollectionFactory;

    /**
     * @var \Summa\Test\Api\EmployeeRepositoryInterface
     */
    private \Summa\Test\Api\EmployeeRepositoryInterface $_employeeRepository;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Summa\Test\Model\ResourceModel\Employee\CollectionFactory $employeeCollectionFactory,
        \Summa\Test\Api\EmployeeRepositoryInterface $employeeRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_employeeCollectionFactory = $employeeCollectionFactory;
        $this->_employeeRepository = $employeeRepository;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }


    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Summa\Test\Model\ResourceModel\Company::class);
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId()
    {
        return $this->getData(self::COMPANY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCompanyId($companyId)
    {
        return $this->setData(self::COMPANY_ID, $companyId);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getEmployees()
    {
        return $this->getData(self::EMPLOYEES);
    }

    /**
     * @inheritDoc
     */
    public function setEmployees($employees)
    {
        return $this->setData(self::EMPLOYEES, $employees);
    }

    /**
     * @inheritDoc
     */
    public function getAverageAge()
    {
        return $this->getData(self::AVERAGE_AGE);
    }

    /**
     * @inheritDoc
     */
    public function setAverageAge($averageAge)
    {
        return $this->setData(self::AVERAGE_AGE, $averageAge);
    }

    /**
     * @inheritDoc
     */
    public function addEmployee($employeeId)
    {
        $result = false;
        try {
            $employee = $this->_employeeRepository->get($employeeId);
            if ($employee->getCompanyId() !== $this->getCompanyId()) {
                $employee->setCompanyId($this->getCompanyId());
                $this->_employeeRepository->save($employee);
            }
            $employees = $this->getEmployeeList();
            $this->setEmployees($employees->count());
            $this->setAverageAge($this->getAverageAgeOfEmployees($employees));
            $this->save();
            $result = true;
        } catch (\Exception $e) {
            // Add log
            return false;
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeList()
    {
        return $this->_employeeCollectionFactory->create()
            ->addFieldToFilter('company_id', ['eq' => $this->getCompanyId()]);
    }

    /**
     * @inheritDoc
     */
    public function searchemployee($employeeId)
    {
        $employees = $this->_employeeCollectionFactory->create()
            ->addFieldToFilter('employee_id', ['eq' => $employeeId])
            ->addFieldToFilter('company_id', ['eq' => $this->getCompanyId()]);
        return $employees->count() > 0 ? $employees->getFirstItem() : null;
    }

    /**
     * @inheritDoc
     */
    public function getAverageAgeOfEmployees($employeeList = null)
    {
        $average = 0;
        $sumAges = 0;
        $employees = $employeeList ?: $this->getEmployeeList();
        if ($employees->count() > 0) {
            foreach ($employees as $employee) {
                $sumAges += $employee->getAge();
            }
            $average = (float)$sumAges / (float)$employees->count();
        }
        return $average;
    }
}

