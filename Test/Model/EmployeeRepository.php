<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Summa\Test\Api\Data\EmployeeInterface;
use Summa\Test\Api\Data\EmployeeInterfaceFactory;
use Summa\Test\Api\Data\EmployeeSearchResultsInterfaceFactory;
use Summa\Test\Api\EmployeeRepositoryInterface;
use Summa\Test\Model\ResourceModel\Employee as ResourceEmployee;
use Summa\Test\Model\ResourceModel\Employee\CollectionFactory as EmployeeCollectionFactory;

class EmployeeRepository implements EmployeeRepositoryInterface
{

    /**
     * @var EmployeeCollectionFactory
     */
    protected $employeeCollectionFactory;

    /**
     * @var Employee
     */
    protected $searchResultsFactory;

    /**
     * @var EmployeeInterfaceFactory
     */
    protected $employeeFactory;

    /**
     * @var ResourceEmployee
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    /**
     * @param ResourceEmployee $resource
     * @param EmployeeInterfaceFactory $employeeFactory
     * @param EmployeeCollectionFactory $employeeCollectionFactory
     * @param EmployeeSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceEmployee $resource,
        EmployeeInterfaceFactory $employeeFactory,
        EmployeeCollectionFactory $employeeCollectionFactory,
        EmployeeSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->employeeFactory = $employeeFactory;
        $this->employeeCollectionFactory = $employeeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(EmployeeInterface $employee)
    {
        try {
            $this->resource->save($employee);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the employee: %1',
                $exception->getMessage()
            ));
        }
        return $employee;
    }

    /**
     * @inheritDoc
     */
    public function get($employeeId)
    {
        $employee = $this->employeeFactory->create();
        $this->resource->load($employee, $employeeId);
        if (!$employee->getId()) {
            throw new NoSuchEntityException(__('Employee with id "%1" does not exist.', $employeeId));
        }
        return $employee;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->employeeCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(EmployeeInterface $employee)
    {
        try {
            $employeeModel = $this->employeeFactory->create();
            $this->resource->load($employeeModel, $employee->getEmployeeId());
            $this->resource->delete($employeeModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Employee: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($employeeId)
    {
        return $this->delete($this->get($employeeId));
    }
}

