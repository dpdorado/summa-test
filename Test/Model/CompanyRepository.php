<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Summa\Test\Api\CompanyRepositoryInterface;
use Summa\Test\Api\Data\CompanyInterface;
use Summa\Test\Api\Data\CompanyInterfaceFactory;
use Summa\Test\Api\Data\CompanySearchResultsInterfaceFactory;
use Summa\Test\Model\ResourceModel\Company as ResourceCompany;
use Summa\Test\Model\ResourceModel\Company\CollectionFactory as CompanyCollectionFactory;

class CompanyRepository implements CompanyRepositoryInterface
{

    /**
     * @var CompanyInterfaceFactory
     */
    protected $companyFactory;

    /**
     * @var Company
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceCompany
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var CompanyCollectionFactory
     */
    protected $companyCollectionFactory;


    /**
     * @param ResourceCompany $resource
     * @param CompanyInterfaceFactory $companyFactory
     * @param CompanyCollectionFactory $companyCollectionFactory
     * @param CompanySearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceCompany $resource,
        CompanyInterfaceFactory $companyFactory,
        CompanyCollectionFactory $companyCollectionFactory,
        CompanySearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->companyFactory = $companyFactory;
        $this->companyCollectionFactory = $companyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(CompanyInterface $company)
    {
        try {
            $this->resource->save($company);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the company: %1',
                $exception->getMessage()
            ));
        }
        return $company;
    }

    /**
     * @inheritDoc
     */
    public function get($companyId)
    {
        $company = $this->companyFactory->create();
        $this->resource->load($company, $companyId);
        if (!$company->getId()) {
            throw new NoSuchEntityException(__('Company with id "%1" does not exist.', $companyId));
        }
        return $company;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->companyCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(CompanyInterface $company)
    {
        try {
            $companyModel = $this->companyFactory->create();
            $this->resource->load($companyModel, $company->getCompanyId());
            $this->resource->delete($companyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Company: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($companyId)
    {
        return $this->delete($this->get($companyId));
    }
}

