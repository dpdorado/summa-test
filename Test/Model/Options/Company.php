<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model\Options;

class Company implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var \Summa\Test\Model\ResourceModel\Company\CollectionFactory
     */
    private \Summa\Test\Model\ResourceModel\Company\CollectionFactory $_companyCollectionFactory;

    /**
     * @param \Summa\Test\Model\ResourceModel\Company\CollectionFactory $companyCollectionFactory
     */
    public function __construct
    (
        \Summa\Test\Model\ResourceModel\Company\CollectionFactory $companyCollectionFactory
    ) {
        $this->_companyCollectionFactory = $companyCollectionFactory;
    }

    /**
     * @return array[]
     */
    public function toOptionArray(): array
    {
        $options = [];

        $options[] = [
            'label' => __('Please select type designer'),
            'value' => ''
        ];

        $companies = $this->_companyCollectionFactory->create();

        if ($companies->count() > 0 ) {
            foreach ($companies as $company) {
                $options[] = [
                    'label' => __($company->getName()),
                    'value' => $company->getCompanyId()
                ];
            }
        }

        return $options;
    }
}
