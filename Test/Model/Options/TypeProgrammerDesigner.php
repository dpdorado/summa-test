<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model\Options;

class TypeProgrammerDesigner implements \Magento\Framework\Data\OptionSourceInterface
{
    const TYPE_PROGRAMMER = [
        'php' => 'PHP',
        'net' => 'NET',
        'python' => 'Python'
    ];
    const TYPE_DESIGNER = [
        'grafico' => 'Gráfico',
        'web' => 'Web'
    ];
    /**
     * @return array[]
     */
    public function toOptionArray(): array
    {
        $options = [];

        $options[] = [
            'label' => __('Please select type'),
            'value' => ''
        ];

        foreach (self::TYPE_PROGRAMMER as $type => $label) {
            $options[] = [
                'label' => __($label),
                'value' => $type,
                'type' => 'programador'
            ];
        }
        foreach (self::TYPE_DESIGNER as $type => $label) {
            $options[] = [
                'label' => __($label),
                'value' => $type,
                'type' => 'diseñador'
            ];
        }
        return $options;
    }
}
