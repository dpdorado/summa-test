<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model\Options;

class TypeEmployee implements \Magento\Framework\Data\OptionSourceInterface
{
    const TYPE_EMPLOYEE = [
        'programador' => 'Programador',
        'diseñador' => 'Diseñador'
    ];
    /**
     * @return array[]
     */
    public function toOptionArray(): array
    {
        $options = [];

        $options[] = [
            'label' => __('Please select type employee'),
            'value' => ''
        ];

        foreach (self::TYPE_EMPLOYEE as $type => $label) {
            $options[] = [
                'label' => __($label),
                'value' => $type
            ];
        }
        return $options;
    }
}
