<?php
/**
 * Copyright © Summa All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Summa\Test\Model\ResourceModel\Employee;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'employee_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Summa\Test\Model\Employee::class,
            \Summa\Test\Model\ResourceModel\Employee::class
        );
    }
}

