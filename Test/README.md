# Mage2 Module Summa Test

    ``summa/module-test``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Summa test - Module that allows the management of companies and their employees.

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Summa`
 - Enable the module by running `php bin/magento module:enable Summa_Test`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require summa/module-test`
 - enable the module by running `php bin/magento module:enable Summa_Test`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration



## Specifications

 - Model - Company, Table - summa_test_company
	- company_id
   - name
   - employees
   - average_age

 - Model - Employee, Table - summa_test_employee
	- employee_id
   - firstname
   - lastname
   - age
   - ability
   - type
   - company_id  #Should be changed to a list


## Prueba

- Se decide crear un módulo en magento en el cual hacer el ejercicio completo con los modelos Empresa y Empleado (Tablas en base de datos, Modelos, Interfaces, Fábricas, Colecciones, etc. ).

-  Se crean las tablas summa_test_company y summa_test_employee con una relación foránea en summa_test_employee que apunta al id de la tabla summa_test_company. 

- Se crean tanto las interfaces como los repositorios de los modelos y sus implementaciones en donde hacemos uso de la herencia al extender en nuestras clases de las clases de magento y hacemos uso de la implementación de interfaces (api). 

- Se crea una Fábrica genérica(Summa\Test\Model\EmployeeFactory) para la creación de empleados que es usada al guardar un empleado desde el administrador de magento.

![IMAGE_DESCRIPTION](screenshot/factory_employee_method.PNG)

- El modelo (Summa\Test\Model\Company) extiende de AbstractModel (clase de magento) e implementa Summa\Test\Api\Data\CompanyInterface (interface propia) la cual tiene métodos getters, setters y los métodos adicionales para poder agregar un empleado, obtener la lista de empleados, buscar un empleado y obtener un promedio de edad de los empleados de la empresa.

![IMAGE_DESCRIPTION](screenshot/add_employee.PNG)

![IMAGE_DESCRIPTION](screenshot/employee_list_employee_search.PNG)

![IMAGE_DESCRIPTION](screenshot/average_age_of_employees.PNG)


## Conclusiones

- En el módulo creado se demuestra conocimientos en la aplicación de los siguientes conceptos: manejo de clases, objetos, fábricas, getters, setters, listados, herencia, implementación de interfaces, además del conocimiento en magento.

## Screenshot

![IMAGE_DESCRIPTION](screenshot/main.PNG)

![IMAGE_DESCRIPTION](screenshot/company_list.PNG)

![IMAGE_DESCRIPTION](screenshot/company_edit.PNG)

![IMAGE_DESCRIPTION](screenshot/employee_list.PNG)

![IMAGE_DESCRIPTION](screenshot/employee_edit.PNG)
