/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/form/element/select'
], function (City) {
    'use strict';

    return City.extend({
        defaults: {
            imports: {
                update: '${ $.provider }:${ $.parentScope }.type',
            }
        },

        initialize: function() {
            this._super();
            return this;
        }
    });
});
